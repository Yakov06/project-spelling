﻿#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <sstream>
#include <vector>
using namespace std;

bool f1(string s1, string s2) {
    //"онажды"
    if (s2.size() + 1 != s1.size() && s2.size() > 1) return false;
    string temp = "";
    int count = 0;
    for (int i = 0; i < s1.size(); i++) {
        bool status = 0;
        for (int j = 0; j < s2.size(); j++) {
            if (s1[i] == s2[j]) {
                status = 1;
                break;
            }
        }
        if (status == 0) count++;
        else {
            temp += s1[i];
        }
    }
    return (s2 == temp) && (count == 1);
}
bool f2(string s1, string s2) {
    // ондажды
    if (s2.size() != s1.size()) return false;
    int count = 0;
    int indexes[2];
    for (int i = 0; i < s1.size(); i++) {
        if (s1[i] != s2[i]) {
            if (count > 1) return false;
            indexes[count] = i;
            count++;
        }
    }
    if (indexes[1] - indexes[0] != 1) return false;
    if (s2[indexes[0]] == s1[indexes[1]] && s1[indexes[0]] == s2[indexes[1]]) return true;
    return false;
}
bool f3(string s1, string s2) {
    // оанажды
    // привет
    // пока
    int count = 0;
    if (s1.size() != s2.size()) return false;
    for (int i = 0; i < s1.size(); i++) {
        if (s1[i] != s2[i]) {
            if (count > 0) return false;
            count++;
        }                             
    }
    if (count == 1) return true;
}
bool f4(string s1, string s2) {
    // одднажды
    if (s2.size() != s1.size() + 1) return false;
    if (s2.size() < 2) return false;
    int count = 0;
    int i = 0;
    for (; i < s1.size(); i++) {
        if (s1[i] != s2[i]) {
            count++;
            break;
        }
    }
    if (count == 0) {
        if (s2[i] == s2[i - 1]) return true;
        return false;
    }
    if (i == 0) return false;

    if (s2[i] != s2[i + 1] && s2[i] != s2[i - 1]) return false;

    for (; i < s1.size(); i++) {
        if (s1[i] != s2[i + 1]) return false;
    }
    return true;
}
int main() {
    setlocale(LC_ALL, "Russian");
    ifstream fin;
    fin.open("1grams-3.txt");
    string slovo;
    string correct_pred;
    set<string> dictionary;

    while (fin >> slovo) {
        fin >> slovo;
        dictionary.insert(slovo);
    }
    // Я посмотрел в Окно.
//    int count = 1;
//"однажды"
//"онажды","ондажды","оанажды","одднажды"
// f1(string s1, string s2)   f2()        f3()    f4()    
    stringstream predlozhenie;
    predlozhenie << "Язык индусс.";
    while (predlozhenie >> slovo) {
        if (slovo == "-") cout << char(45);
        char lch = 'p';
        if (slovo[slovo.size() - 1] == '.' ||
            slovo[slovo.size() - 1] == ',' ||
            slovo[slovo.size() - 1] == '!' || 
            slovo[slovo.size() - 1] == '?' || 
            slovo[slovo.size() - 1] == ':' ||
            slovo[slovo.size() - 1] == ';') {
            lch = slovo[slovo.size() - 1];
            slovo.pop_back();
        }
        /*cout << slovo << endl;*/
        // Сделать все слова с маленькой буквы и без знаков препинания
        if (dictionary.find(slovo) != dictionary.end()) {
            if (lch != 'p') {
                correct_pred += slovo;
                correct_pred += lch;
                system("cls");
                continue;
            }
            correct_pred += slovo;
            correct_pred += " ";
            system("cls");
            cout << correct_pred;
            continue;
        }
        vector<string> words;
        //здесь пробегаемся
        for (auto it = dictionary.begin(); it != dictionary.end(); it++) {
            if (f1(*it, slovo) || f2(*it, slovo) || f3(*it, slovo) || f4(*it, slovo)) {
                slovo = *it;
                words.push_back(slovo);
                /*               cout << slovo;*/
                if (lch != 'p') {
                    correct_pred += lch;
                    break;
                }
                /*break;*/
            }
        }
        cout << "       WORDS: " << endl;
        for (int i = 0; i < words.size(); i++) {
            cout << i + 1 << ". " << words[i] << endl;
        }
        cout << "SIZE: " << words.size() << endl;
        if (words.size() == 0) {
            correct_pred += slovo;
            correct_pred += " ";
            system("cls");
            continue;
        }
        if (words.size() == 1) {
            correct_pred += words[0];
            correct_pred += " ";
            system("cls");
            continue;
        }
        int index;
        cin >> index;
        if (index >= words.size() || index == 0) {
            correct_pred += words[0];
            correct_pred += " ";
            system("cls");
            cout << correct_pred;
            continue;
        };
        //cout << words[index] << " ";
        correct_pred += words[index - 1];
        correct_pred += " ";
        system("cls");
        cout << correct_pred;
        continue;
    }
    cout << correct_pred;
}
